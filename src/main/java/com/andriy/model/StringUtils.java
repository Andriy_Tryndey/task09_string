package com.andriy.model;

public class StringUtils {
    public String text;

    public StringUtils(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return  text;
    }
}
