package com.andriy.view;

public interface Printable {
    void print();
}
