package com.andriy.view;

import com.andriy.model.StringUtils;

import java.util.*;

public class MyView {
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    StringBuilder sb = new StringBuilder();
    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu(){
        menu = new LinkedHashMap<>();

        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("Q", bundle.getString("Q"));
    }

    public MyView(){
        locale = new Locale("ua");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::testStringUtils);
        methodsMenu.put("2", this::internationallizeMenuUkrainian);
        methodsMenu.put("3", this::internationallizeMenuEnglish);
        methodsMenu.put("4", this::internationallizeMenuJapanese);
        methodsMenu.put("5", this::internationallizeMenuTurkish);
        methodsMenu.put("6", this::internationallizeMenuEgipt);
        methodsMenu.put("7", this::internationallizeMenuItalian);
        methodsMenu.put("8", this::internationallizeMenuGreek);
        methodsMenu.put("9", this::internationallizeMenuSpanish);
    }

    private void testStringUtils() {

        System.out.println("Введіть текст");
        StringUtils stringUtils = new StringUtils(sc.nextLine());
        sb.append(stringUtils);
        System.out.println(sb);
    }

    private void internationallizeMenuUkrainian() {
        locale = new Locale("ua");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            }catch (Exception e) {

            }
        }while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU");
        for (String str : menu.values()){
            System.out.println(str);
        }
    }

    private void internationallizeMenuEnglish() {
        locale = new Locale("gb");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationallizeMenuJapanese() {
        locale = new Locale("j");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationallizeMenuTurkish() {
        locale = new Locale("tr");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationallizeMenuEgipt() {
        locale = new Locale("e");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationallizeMenuItalian() {
        locale = new Locale("i");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationallizeMenuGreek() {
        locale = new Locale("gr");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationallizeMenuSpanish() {
        locale = new Locale("e");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }
}
